package com.test;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class UserController {
	
	@RequestMapping("/login")
	public String showUserPage()
	{
		return "userForm";
	}
	
	@RequestMapping("/submitform")
	public String submitUserForm(HttpServletRequest request, Model model)
	{
		
		String user = request.getParameter("user");
		
		String pass = request.getParameter("pwd");
		
		User obj = new User();
		
		obj.setUserName(user);
		obj.setPassword(pass);
		
		model.addAttribute("info", obj);
		
		return "submitForm";
	}
	
	@RequestMapping("/submitform1")
	public String submitUserForm1(@RequestParam("user")String user, @RequestParam("pwd") String pass, Model model)
	{
		
		//String user = request.getParameter("user");
		
		//String pass = request.getParameter("pwd");
		
		User obj = new User();
		
		obj.setUserName(user);
		obj.setPassword(pass);
		
		model.addAttribute("info", obj);
		
		return "submitForm";
	}

}
