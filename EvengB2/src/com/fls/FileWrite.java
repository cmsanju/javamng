package com.fls;

import java.io.FileWriter;

public class FileWrite {
	
	public static void main(String[] args)throws Exception
	{
		
		FileWriter fw = new FileWriter("src/sample.txt");
		
		String msg = "Hi this is char stream file write and read operations";
		
		fw.write(msg);
		
		fw.flush();
		
		System.out.println("Done.");
	}

}
