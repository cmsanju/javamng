package com.fls;

import java.io.FileOutputStream;
import java.io.ObjectOutputStream;

public class SerialDemo {
	
	public static void main(String[] args) throws Exception
	{
		
		FileOutputStream fos = new FileOutputStream("src/customer.txt");
		
		ObjectOutputStream obj = new ObjectOutputStream(fos);
		
		Customer cst = new Customer();
		
		cst.id = 1111;
		cst.name = "Java";
		cst.city = "Blr";
		cst.pinc = 123123;
		
		obj.writeObject(cst);
		
		System.out.println("Done.");
	}

}
