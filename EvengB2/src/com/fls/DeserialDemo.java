package com.fls;

import java.io.FileInputStream;
import java.io.ObjectInputStream;

public class DeserialDemo {
	
	public static void main(String[] args) throws Exception
	{
		
		FileInputStream fis = new FileInputStream("src/customer.txt");
		
		ObjectInputStream obj = new ObjectInputStream(fis);
		
		Customer ct = (Customer)obj.readObject();
		
		System.out.println(ct.id+" "+ct.name+" "+ct.city+" "+ct.pinc);
		
		
	}

}
