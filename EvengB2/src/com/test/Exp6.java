package com.test;

interface Inf1
{
	void add();
}

interface Inf2
{
	void sub();
}

class Impl implements Inf1,Inf2
{
	public void add()
	{
		System.out.println("add()");
	}
	
	public void sub()
	{
		System.out.println("sub()");
	}
}

public class Exp6 {
	
	public static void main(String[] args) {
		
		Impl obj = new Impl();
		
		obj.add();
		obj.sub();
		
	}

}
