package com.test;

interface Inf
{
	void cat();
	void dog();
	
	default void show()
	{
		System.out.println("default");
	}
	
	static void disp()
	{
		System.out.println("static");
	}
}

abstract class Abs
{
	public abstract void human();
	
	public void animal()
	{
		System.out.println("abs normal");
	}
	
}

class Normal extends Abs implements Inf
{
	//@Override
	public void book()
	{
		System.out.println("normal class");
	}
	
	@Override
	public void human()
	{
		System.out.println("abs overrided");
	}
	
	@Override
	public void cat()
	{
		System.out.println("inf overrided m2");
	}
	@Override
	public void dog()
	{
		System.out.println("inf overrided m1");
	}
}

public class Exp5 {
	
	public static void main(String[] args) {
		
		Normal obj = new Normal();
		
		obj.animal();
		obj.book();
		obj.cat();
		obj.dog();
		obj.human();
		obj.show();
		
		Inf.disp();
	}

}
