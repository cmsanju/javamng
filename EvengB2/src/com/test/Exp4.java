package com.test;


class Parent
{
   public void draw()
   {
	   System.out.println("Tle");
   }
}

class Child1 extends Parent
{
	@Override
	public void draw()
	{
		System.out.println("cle");
	}
}

class Child2 extends Parent
{
	@Override
	public void draw()
	{
		System.out.println("rle");
	}
}

public class Exp4 {
	
	public static void main(String[] args) {
		
		Parent c = new Child1();
		
		c.draw();
		
		Parent c1 = new Child2();
		
		c1.draw();
	}

}
