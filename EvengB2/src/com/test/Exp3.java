package com.test;

class F
{
	public void human()
	{
		System.out.println("parent");
	}
	
}

class G extends F
{
	public void animal()
	{
		System.out.println("animal");
	}
}

class H extends F
{
	public void dog()
	{
		System.out.println("dog");
	}
}

public class Exp3 {
	
	public static void main(String[] args) {
		
		G g = new G();
		
		g.animal();
		g.human();
		
		H h = new H();
		
		h.human();
		h.dog();	
	}

}
