package com.test;


class A
{
	int id = 101;
	String name = "Java";
	
	public void show()
	{
		System.out.println("parent");
	}
}

class B extends A
{
	//A a = new A();
	
	String city = "MPL";
	
	public void disp()
	{
		System.out.println(id+" "+name+" "+city);
	}
}


public class Exp1 {
	
	public static void main(String[] args) {
		
		B b = new B();
		
		b.show();
		b.disp();
		
	}

}
