package com.test;

public class Exp9 {
	
	public static void main(String[] args) {
		
		String str = "100";
		
		int x = Integer.parseInt(str);
		
		int y = 200;
		
		String val = String.valueOf(x);
		
		//auto boxing
		
		int j = 200;
		
		Integer jj = new Integer(j);
		
		long l = 49449;
		
		Long ll = new Long(l);
		
		//auto unboxing
		
		Float f = new Float(74.48);
		
		float ff = f;
		
		Double d = new Double(447.488);
		
		double dd = d;
		
	}

}
