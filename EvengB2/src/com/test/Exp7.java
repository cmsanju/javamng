package com.test;

interface Inf3
{
	void mul();
	
	interface Inf4
	{
	 void div();
	}
}

class Impl1 implements Inf3.Inf4
{
	public void div()
	{
		System.out.println("div overrided");
	}
	
	
}

public class Exp7 {
	
	public static void main(String[] args) {
		
		Impl1 obj = new Impl1();
		
		obj.div();
				
	}

}
