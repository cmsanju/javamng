package com.test;

class C
{
	public void cat()
	{
		System.out.println("cat");
	}
}

class D extends C
{
	public void dog()
	{
		System.out.println("dog");
	}
}

class E extends D
{
	public void fox()
	{
		System.out.println("fox");
	}
}

public class Exp2 {
	
	public static void main(String[] args) {
		
		E e = new E();
		
		e.cat();
		e.dog();
		e.fox();
	}

}
