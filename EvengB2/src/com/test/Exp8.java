package com.test;

@FunctionalInterface
interface Inf5
{
	void msg();
	
	default void hello()
	{
		
	}
	
	static void sayHello()
	{
		
	}
}

public class Exp8 {
	
	public static void main(String[] args) {
		
		Inf5 obj = new Inf5()
				{
					public void msg()
					{
						System.out.println("overrided fun inf");
					}
				};
				
				obj.msg();
				
		new Inf5() {
			
			public void msg()
			{
				System.out.println("nameless object");
			}
		}.msg();
		
		//jdk 1.8 feature 
		
		Inf5 obj1 = () -> System.out.println("lambda expression");
		
		obj1.msg();
	}

}
