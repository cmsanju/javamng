package com.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class Exp1 {
	
	public static void main(String[] args)throws Exception
	{
		
		//1 load the driver class
		//Class.forName("com.mysql.jdbc.Driver");
		
		//2 Create connection object 
		Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/evngfeb", "root", "password");
		
		//3 create statement object
		Statement stmt  = con.createStatement();
		
		//String sql = "create table emp1(id int, name varchar(50), city varchar(50))";
		
		//String sql = "insert into emp1 values(2, 'test', 'MPL')";
		
		//String sql = "update emp1 set name ='java' where id = 1 ";
		
		//String sql = "delete from emp1 where id = 1";
		
		String sql = "select * from employee";
		
		//4 execute query
		ResultSet rs =   stmt.executeQuery(sql);
		
		while(rs.next())
		{
			System.out.println("ID : "+rs.getInt(1)+" Name : "+rs.getString(2)+" City : "+rs.getString(3));
		}
		
		//5 close the connection object
		con.close();
	}

}
