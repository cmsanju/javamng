package com.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;

public class Exp2 {
	
	public static void main(String[] args) throws Exception
	{
		
		Class.forName("com.mysql.jdbc.Driver");
		
		Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/evngfeb", "root", "password");
		/*
		PreparedStatement pst = con.prepareStatement("insert into emp1 values(?,?,?)");
		
		pst.setInt(1, 111);
		pst.setString(2, "APPLE");
		pst.setString(3, "TPT");
		
		pst.execute();
		
		PreparedStatement pst = con.prepareStatement("update emp1 set name = ? where id = ?");
		
		pst.setString(1, "angeer");
		pst.setInt(2, 111);
		
		pst.execute();
		
		
		
		PreparedStatement pst = con.prepareStatement("delete from emp1 where id = ?");
		
		pst.setInt(1, 111);
		
		pst.execute();
		
		*/
		
		PreparedStatement pst = con.prepareStatement("select * from emp1");
		
		ResultSet rs = pst.executeQuery();
		
		ResultSetMetaData rsd = rs.getMetaData();
		
		System.out.println(rsd.getColumnCount());
		
		while(rs.next())
		{
			System.out.println(rs.getInt(1)+" "+rs.getString(2)+" "+rs.getString(3));
		}
		
		System.out.println("Done");
		
	}

}
