package com.excp;

public class Exp2 {
	
	public static void main(String[] args) {
		
		try
		{
			System.out.println(100/5);
			
			String str = "java";
			
			System.out.println(str.charAt(1));
			
			int[] ar = {12,34,56,78};
			
			System.out.println(ar[2]);
			
			String name = "admin";
			
			System.out.println(name.equals("admin"));
			
			String s = "100";
			
			int y = Integer.parseInt(s);
		}
		
		catch(ArithmeticException ae)
		{
			System.out.println("can't divided by zero");
		}
		catch(StringIndexOutOfBoundsException sie)
		{
			System.out.println("check string length");
		}
		catch(ArrayIndexOutOfBoundsException aie)
		{
			System.out.println("check array size");
		}
		catch(NullPointerException npe)
		{
			System.out.println("please enter name");
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
			
			System.out.println(e);
			
			e.printStackTrace();
		}
		
		finally
		{
			System.out.println("i am from finally block");
		}
		
	}

}
