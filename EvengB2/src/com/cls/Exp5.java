package com.cls;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

public class Exp5 {
	
	public static void main(String[] args) {
		
		//Map<String, Integer> data = new HashMap<String, Integer>();
		
		//Map<String, Integer> data = new LinkedHashMap<String, Integer>();
		
		Map<String, Integer> data = new TreeMap<>();
		
		data.put("lenovo", 234);
		data.put("sony", 324);
		data.put("asus", 234);
		data.put("dell", 387);
		data.put("apple", 7763);
		data.put("acer", 776);
		data.put("sony", 5674);
		
		System.out.println(data);
		
		Iterator<Entry<String, Integer>> itr = data.entrySet().iterator();
		
		while(itr.hasNext())
		{
			Entry<String, Integer> et = itr.next();
			
			System.out.println("Key : "+et.getKey()+" Value : "+et.getValue());
		}
		
		System.out.println(data.get("sony"));
	}

}
