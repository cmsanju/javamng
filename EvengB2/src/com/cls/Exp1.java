package com.cls;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

public class Exp1 {
	
	public static void main(String[] args) {
		
		//List data = new ArrayList();
		
		List data = new LinkedList();
		
		data.add(10);
		data.add("java");
		data.add(32.33);
		data.add(99.22f);
		data.add('A');
		data.add("java");
		data.add(10);
		
		System.out.println(data);
		
		System.out.println(data.size());
		
		//Iterator , ListIterator, Enumeration
		
		//Iterator itr = data.iterator();
		
		ListIterator itr = data.listIterator();
		
		while(itr.hasNext())
		{
			System.out.println(itr.next());
		}
		
		System.out.println("---------");
		
		while(itr.hasPrevious()) {
			
			System.out.println(itr.previous());
		}
	}
}
