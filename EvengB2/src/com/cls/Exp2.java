package com.cls;

import java.util.Stack;

public class Exp2 {
	
	public static void main(String[] args) {
		
		Stack data = new Stack();
		
		data.add(10);
		data.add("java");
		data.add(32.33);
		data.add(99.22f);
		data.add('A');
		data.add("java");
		data.add("hello");
		
		
		System.out.println(data);
		
		System.out.println(data.peek());
		
		data.push("spring");
		
		System.out.println(data.peek());
		
		System.out.println(data.pop());
		
		System.out.println(data);
		
		System.out.println(data.empty());
		
		System.out.println(data.search(100));
	}

}
