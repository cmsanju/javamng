package com.cls;

import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;

public class Exp3 {
	
	public static void main(String[] args) {
		
		Set data = new HashSet();
		
		data.add(10);
		data.add("java");
		data.add(32.33);
		data.add(99.22f);
		data.add('A');
		data.add("java");
		data.add(10);
		
		System.out.println(data);
		
		Iterator itr = data.iterator();
		
		while(itr.hasNext())
		{
			System.out.println(itr.next());
		}
		
		Set dat = new LinkedHashSet();
		
		dat.add(10);
		dat.add("java");
		dat.add(32.33);
		dat.add(99.22f);
		dat.add('A');
		dat.add("java");
		dat.add(10);
		
		System.out.println(dat);
	}

}
