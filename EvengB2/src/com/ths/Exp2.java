package com.ths;

public class Exp2 extends Thread
{
	@Override
	public void run()
	{
		System.out.println("i am from run() Bank opts : "+Thread.currentThread().getName());
	}
	public static void main(String[] args) {
		
		Thread t1 = new Exp2();
		
		Thread t2 = new Exp2();
		
		Thread t3 = new Exp2();
		
		System.out.println("Thread default name : "+t1.getName());
		System.out.println("Thread default name : "+t2.getName());
		System.out.println("Trhead default name : "+t3.getName());
		
		t1.setName("Transfer");
		t2.setName("Withdraw");
		t3.setName("Credit");
		
		System.out.println("after setting the name of thread : "+t1.getName());
		
		//t1.start();
		//t2.start();
		//t3.start();
		
		System.out.println("Default thread prority : "+t1.getPriority());
		System.out.println("Default thread priority : "+t2.getPriority());
		
		System.out.println(MAX_PRIORITY);
		System.out.println(NORM_PRIORITY);
		System.out.println(MIN_PRIORITY);
		
		t1.setPriority(MAX_PRIORITY);
		t3.setPriority(MIN_PRIORITY);
		
		System.out.println("after setting the thread priority : "+t1.getPriority());
	}
}
