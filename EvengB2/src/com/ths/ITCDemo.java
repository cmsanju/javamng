package com.ths;

class Item
{
	boolean valSet = false;
	int value;
	
	public synchronized void putItem(int i)
	{
		try {
			if(valSet)
			{
				wait();
			}
			
			value = i;
			
			System.out.print("Producer thread produced -> "+value);
			
			valSet = true;
			
			notify();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public synchronized void getItem()
	{
		try
		{
			if(!valSet)
			{
			  wait();	
			}
			
			System.out.println(" Consumer thread consumed -> "+value);
			
			valSet = false;
			
			notify();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
}

class Producer extends Thread
{
	Item item;
	
	int i;
	
	public Producer(Item item)
	{
		this.item = item;
	}
	@Override
	public void run()
	{
		try
		{
			while(true)
			{
				Thread.sleep(2000);
				
				item.putItem(++i);
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
}

class Consumer extends Thread
{
	Item item;
	
	public Consumer(Item item)
	{
		this.item = item;
	}
	@Override
	public void run()
	{
		try {
			while(true)
			{
				Thread.sleep(500);
				item.getItem();
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
}

public class ITCDemo {
	
	public static void main(String[] args) {
		
		Item item = new Item();
		
		Producer pr = new Producer(item);
		Consumer cr = new Consumer(item);
		
		pr.start();
		cr.start();
		
	}

}
