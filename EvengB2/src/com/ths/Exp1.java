package com.ths;

public class Exp1 extends Thread
{
	@Override
	public void run()
	{
		try {
			Thread.sleep(1000);
			System.out.println("i am from run");
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	public static void main(String[] args) throws Exception
	{
		
		Exp1 t1 = new Exp1();
		
		System.out.println("before starting thread state : "+t1.getState());
		
		System.out.println("before starting thread status : "+t1.isAlive());
		
		t1.start();
		
		System.out.println("after starting thread state : "+t1.getState());
		System.out.println("after starting thread status : "+t1.isAlive());
		
		Thread.sleep(100);
		
		System.out.println("in sleep thread state : "+t1.getState());
		System.out.println("ins sleep thread status : "+t1.isAlive());
		
		t1.join();
		
		System.out.println("after joining thread state : "+t1.getState());
		System.out.println("after joining thread status : "+t1.isAlive());
		
	}

}
