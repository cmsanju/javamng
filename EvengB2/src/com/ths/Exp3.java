package com.ths;

class Add
{
	public void add()
	{
		System.out.println("add method");
	}
}

class Sub
{
	public void sub()
	{
		System.out.println("sub method");
	}
}

public class Exp3 implements Runnable
{

	@Override
	public void run() {
		
		try
		{
			Add a = new Add();
			
			a.add();
			Thread.sleep(2000);
			Sub s = new Sub();
			
			s.sub();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
	}
	
	public static void main(String[] args) {
		
		Exp3 r = new Exp3();
		
		//r.start();
		
		ThreadGroup tg1 = new ThreadGroup("Bank");
		
		Thread t1 = new Thread(tg1, r, "Transfer");
		Thread t2 = new Thread(tg1, r, "Withdraw");
		Thread t3 = new Thread(tg1, r, "Credit");
		
		ThreadGroup tg2 = new ThreadGroup("Maths");
		
		Thread t4 = new Thread(tg2, r, "ADD");
		Thread t5 = new Thread(tg2, r, "SUB");
		Thread t6 = new Thread(tg2, r, "DIV");
		
		//tg1.start();
		
		t1.start();
		t3.start();
		System.out.println("TG1 : "+tg1.activeCount());
		
		t5.start();
		t6.start();
		System.out.println("TG2 : "+tg2.activeCount());
		
		System.out.println(t1.getName());
	}
	
}
