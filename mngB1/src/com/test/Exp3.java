package com.test;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Exp3 {
	
	public static void main(String[] args) {
		
		System.out.println(Pattern.matches("java", "Java"));
		
		Pattern pt = Pattern.compile("javafsd");
		
		String vr = "javafsdhello";
		
		Matcher mt = pt.matcher(vr);
		
		while(mt.find())
			System.out.println(mt.start()+" To "+(mt.end()-1));
		
		Pattern p1 = Pattern.compile("..a");
		
		Matcher m1 = p1.matcher("ssa");
		
		System.out.println(m1.matches());
		
		
		System.out.println(Pattern.matches("[a-zA-Z0-9]{8}", "java12"));
		
		String psd = "^(?=.*[0-9])"+"(?=.*[a-z])(?=.*[A-Z])"+"(?=.*[#@$%&])"+"(?=\\s+$).{8,20}$";
		
		Pattern pp = Pattern.compile(psd);
		
		
	}

}
