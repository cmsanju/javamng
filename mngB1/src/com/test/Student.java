package com.test;

public class Student {
	
	//default constructor
	
	public  Student()
	{
		System.out.println("default constructor");
	}
	
	// parameterised constructor
	
	public Student(int x, String name)
	{
		System.out.println("parameterised");
	}
	
	//overloaded constructor
	
	public Student(double x)
	{
		System.out.println("overloaded");
	}
	
	//object parameterised constructor
	
	public Student(Student obj)
	{
		System.out.println("object parameterised");
	}
	
	public static void main(String[] args) {
		
		Student std1 = new Student();
		Student std2 = new Student(33,"Java");
		Student std3 = new Student(34.56);
		Student std4 = new Student(std1);
	}
}
