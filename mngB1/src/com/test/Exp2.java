package com.test;

public class Exp2 {
	
	public static void main(String[] args) {
		
		int[] ar = {10,20,30,40,50,60};
		
		System.out.println(ar.length);
		
		System.out.println(ar[0]);
		System.out.println(ar[1]);
		System.out.println(ar[5]);
		
		//System.out.println(ar[7]);
		
		for(int i = 0; i < ar.length; i++)
		{
			System.out.println(ar[i]);
		}
		
		for(int x : ar)
		{
			System.out.println(x);
			
		}
		
		int[] arr = new int[5];
		
		arr[3] = 20;
		
		System.out.println(arr[3]);
		
		int[][] ar1 = {{10,20,30},{40,50,60},{70,80,90}}; 
		
		System.out.println(ar1[0][0]);
		System.out.println(ar1[0][1]);
		
		for(int i = 0; i<3; i++)
		{
			for(int j = 0; j<3;j++)
			{
				System.out.print(ar1[i][j]+" ");
			}
			System.out.println();
		}
			
	}

}
