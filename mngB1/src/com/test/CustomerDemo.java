package com.test;

class Customer
{
	public Customer()
	{
		System.out.println("constructor");//4
	}
	
	{
		System.out.println("instance block");//3
	}
	
	static
	{
		System.out.println("static block");//2
	}
}

public class CustomerDemo {
	
	public static void main(String[] args) {
		
		System.out.println("main method");//1
		
		Customer obj = new Customer();
		
		Customer obj1 = new Customer();
		
	}

}
