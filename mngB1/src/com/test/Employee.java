package com.test;

//POJO
public class Employee {
	
	private int id;
	
	private String name;
	
	private String cmp;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCmp() {
		return cmp;
	}

	public void setCmp(String cmp) {
		this.cmp = cmp;
	}
}

class Exp11 extends Employee
{
	public static void main(String[] args) {
		
		Employee emp = new Employee();
		
		emp.getId();
		emp.setId(99);
	}
}
