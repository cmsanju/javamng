package com.test;

class Book
{
	Book obj = null;
	
	public Book()
	{
		this(20,80);
		System.out.println("Default");//3
	}
	
	public Book(int x,int y)
	{
		this("hello");
		System.out.println("parameterised");//2
	}
	
	public Book(String str)
	{
		
		System.out.println("string data");//1
	}
	
	//factory method
	
	public Book helloObj()
	{
		if(obj == null)
		{
			obj = new Book();
		}
		
		return obj;
	}
}

public class BookDemo {
	
	public static void main(String[] args) {
		
		Book obj = new Book();
		
		Book obj2 = obj.helloObj();
		
		System.out.println("main method");//4
	}

}
