package com.test;

import java.util.Scanner;

public class Test {
	
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		System.out.println("enter id");
		
		int id = sc.nextInt();
		
		System.out.println("enter name");
		
		String name = sc.next();
		
		System.out.println("enter company");
		
		String cmp = sc.next();
		
		Employee emp = new Employee();
		
		emp.setId(id);
		emp.setName(name);
		emp.setCmp(cmp);
		
		System.out.println("Employee Details : "+emp.getId()+" "+emp.getName()+" "+emp.getCmp());
	}

}
