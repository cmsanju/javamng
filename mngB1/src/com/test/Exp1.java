package com.test;

class ItcEmp
{
	ItcEmp obj;
	
	public void disp()
	{
		System.out.println("normal");
	}
	
	public ItcEmp createObj()
	{
		if(obj == null)
		{
			obj = new ItcEmp();
			System.out.println("object created");
		}
		
		return obj;
	}
}

public class Exp1 {
	
	public static void main(String[] args) {
		
		ItcEmp obj = new ItcEmp();
		
		obj.createObj();
		
		String val1 = "400";
		String val2 = "600";
		
		System.out.println(val1+val2);
		
		int x = Integer.parseInt(val1);
		int y = Integer.parseInt(val2);
		
		System.out.println(x+y);
	}

}
