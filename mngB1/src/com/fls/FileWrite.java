package com.fls;

import java.io.File;
import java.io.FileOutputStream;

public class FileWrite {
	
	public static void main(String[] args) throws Exception
	{
		
		File file = new File("src/write.txt");
		
		FileOutputStream fos = new FileOutputStream(file);
		
		String str = "This is simple story to write the data using byte stream";
		
		fos.write(str.getBytes());
		
		System.out.println("Done.");
	}
	
}
