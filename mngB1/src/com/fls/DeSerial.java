package com.fls;

import java.io.FileInputStream;
import java.io.ObjectInputStream;

public class DeSerial {
	
	public static void main(String[] args) throws Exception
	{
		
		FileInputStream fis = new FileInputStream("src/employee.txt");
		
		ObjectInputStream ois = new ObjectInputStream(fis);
		
	    Employee obj =(Employee)ois.readObject();
	    
	    System.out.println(obj.id+" "+obj.name+" "+obj.cmp+" "+obj.city+" "+obj.pin);
	}

}
