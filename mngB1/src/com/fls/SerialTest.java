package com.fls;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;


public class SerialTest {
	
	public static void main(String[] args) throws Exception
	{
		
		FileOutputStream fos = new FileOutputStream("src/employee.txt");
		
		ObjectOutputStream objs = new ObjectOutputStream(fos);
		
		Employee emp = new Employee();
		
		emp.id = 101;
		emp.name = "Java";
		emp.cmp = "Dell";
		emp.city = "bLR";
		emp.pin = 560068;
		
		objs.writeObject(emp);
		
		System.out.println("Done.");
	}

}
