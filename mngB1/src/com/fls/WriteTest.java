package com.fls;

import java.io.File;
import java.io.FileWriter;

public class WriteTest {
	
	public static void main(String[] args) throws Exception
	{
		
		File file = new File("src/sample.txt");
		
		FileWriter fw = new FileWriter(file);
		
		String str = "This is file write operation using char stream";
		
		fw.write(str);
		
		fw.flush();
		
		System.out.println("Done.");
	}

}
