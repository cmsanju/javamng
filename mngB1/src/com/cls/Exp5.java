package com.cls;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

public class Exp5 {
	
	public static void main(String[] args) {
		
		Map<String, Integer> data = new TreeMap<String, Integer>();
		
		data.put("dell", 2000);
		data.put("sony", 4000);
		data.put("lenovo", 90);
		data.put("asus", 200);
		data.put("apple", 5000);
		data.put("apple", 650);
		
		System.out.println(data);
		
		
		Iterator<Entry<String, Integer>> itr = data.entrySet().iterator();
		
		while(itr.hasNext())
		{
			Entry<String, Integer> et = itr.next();
			
			System.out.println("Product : "+et.getKey()+" Price : "+et.getValue());
		}
		
		for(String key : data.keySet())
		{
			System.out.println(key+" "+data.get(key));
		}
		
		for(Integer v : data.values())
		{
			System.out.println(v);
		}
	}

}
