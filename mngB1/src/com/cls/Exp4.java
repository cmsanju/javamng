package com.cls;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

class Customer implements Comparable<Customer>
{
	private int id;
	
	private String name;
	
	private String city;
	
	public int compareTo(Customer obj)
	{
		return this.id-obj.id;
	}
	
	public Customer()
	{
		
	}
	
	public Customer(int id, String name, String city)
	{
		this.id = id;
		this.name = name;
		this.city = city;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}
}

class NameComparator implements Comparator<Customer>
{
	public int compare(Customer obj1, Customer obj2)
	{
		return obj2.getName().compareTo(obj1.getName());
	}
}

public class Exp4 {
	
	public static void main(String[] args) {
		
		
		
		List<Customer> listData = new ArrayList<Customer>();
		
		listData.add(new Customer(11,"java", "blr"));
		
		listData.add(new Customer(22,"spring", "pune"));
		
		listData.add(new Customer(20,"fsd", "kolkata"));
		
		listData.add(new Customer(12,"hibernate", "hyd"));
		
		//Collections.sort(listData);
		
		Collections.sort(listData, new NameComparator());
		
		for(Customer cobj : listData)
		{
			cobj = new Customer();
			
			System.out.println(cobj.getId()+" "+cobj.getName()+" "+cobj.getCity());
			
			cobj.setId(11);
			
			//CRUD 
		}
		
		
		
	}

}
