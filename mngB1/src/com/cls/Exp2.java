package com.cls;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.ListIterator;
import java.util.Set;
import java.util.TreeSet;

public class Exp2 {
	
	public static void main(String[] args) {
		
		List<String> listNames = new ArrayList<String>();
		
		listNames.add("java");
		listNames.add("php");
		listNames.add("spring");
		listNames.add("hello");
		listNames.add("hibernate");
		listNames.add("java");
		
		System.out.println(listNames);
		
		System.out.println(listNames.size());
		
		//Iterator ListIterator  Enumeration
		
	//	Iterator<String> itr = listNames.iterator();
		
		ListIterator<String> itr = listNames.listIterator();
		
		while(itr.hasNext())
		{
			System.out.println(itr.next());
		}
		
		System.out.println("*******");
		
		while(itr.hasPrevious())
		{
			System.out.println(itr.previous());
		}
		
		Set<String> data = new TreeSet<String>();
		

		data.add("java");
		data.add("php");
		data.add("spring");
		data.add("hello");
		data.add("hibernate");
		data.add("java");
		
		System.out.println(data);
	}

}
