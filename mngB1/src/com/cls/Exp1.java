package com.cls;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.TreeMap;
import java.util.TreeSet;

public class Exp1 {
	
	public static void main(String[] args) {
		
		int x = 12;
		
		int[] ar = {12,34};
		
		Collection cl = new ArrayList();
		
		cl.add(12);
		cl.add("java");
		cl.add(34.88);
		cl.add('A');
		cl.add(12);
		cl.add("java");
		cl.add(x);
		
		System.out.println(cl);
		
		HashMap hm = new HashMap<>();
		
		hm.put(1, "jva");
		hm.put("hello", 33);
		hm.put(1, "php");
		hm.put("name", "fsd");
		
		System.out.println(hm);
		
		TreeMap<Integer, String> tm = new TreeMap<Integer, String>();
		
		tm.put(11, "java");
		tm.put(1, "hello");
		tm.put(2, "php");
		
		System.out.println(tm);
		
		TreeSet<String> str = new TreeSet<>();
		
		str.add("xml");
		str.add("html");
		str.add("java");
		str.add("ant");
		
		System.out.println(str);
		
		
	}

}
