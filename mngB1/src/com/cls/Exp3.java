package com.cls;

import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

class Employee 
{
	private int id;
	private String name;
	private String city;
	
	public Employee()
	{
		
	}
	
	public Employee(int id, String name, String city)
	{
		this.id = id;
		this.name = name;
		this.city = city;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
}

public class Exp3 {
	
	public static void main(String[] args) {
		
		List<Employee> empList = new LinkedList<Employee>();
		
		empList.add(new Employee(101,"Surodeep", "Kolkata"));
		
		empList.add(new Employee(111, "Java", "Blr"));
		
		empList.add(new Employee(221,"spring", "pune"));
		
		empList.add(new Employee(331, "Java", "Blr"));
		
		Iterator<Employee> itr = empList.iterator();
		
		while(itr.hasNext())
		{
			Employee emp = itr.next();
			
			System.out.println(emp.getId()+" "+emp.getName()+" "+emp.getCity());
		}
		
		Set<Employee> empData = new HashSet<Employee>();
		
		
		empData.add(new Employee(101,"Surodeep", "Kolkata"));
		
		empData.add(new Employee(111, "Java", "Blr"));
		
		empData.add(new Employee(221,"spring", "pune"));
		
		empData.add(new Employee(331, "Java", "Blr"));
		
		
		Iterator<Employee> itr1 = empData.iterator();
		
		while(itr1.hasNext())
		{
			Employee emp = itr1.next();
			
			System.out.println(emp.getId()+" "+emp.getName()+" "+emp.getCity());
		}
		
	}

}
