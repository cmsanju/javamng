package com.inh;

@FunctionalInterface
interface FunInf
{
	String book(int x);
	
	default void show()
	{
		
	}
	
	static void disp()
	{
		
	}
}



public class Exp7 {
	
	public static void main(String[] args) {
		
		FunInf obj = new FunInf()
				{
					public String book(int x)
					{
						System.out.println("override");
						
						return "hello";
					}
				};
				
				obj.book(10);
				
		FunInf obj1 = (int x) -> {
			
			System.out.println("lambda expression");
			
			return "test";
		};
		
		obj1.book(20);
	}

}
