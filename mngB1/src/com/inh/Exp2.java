package com.inh;

class C
{
	public void cat()
	{
		System.out.println("cat()");
	}
}

class D extends C
{
	public void dog()
	{
		System.out.println("dog()");
	}
}

class E extends D
{
	public void pet()
	{
		System.out.println("pet()");
	}
}

public class Exp2 {
	
	public static void main(String[] args) {
		
		E e = new E();
		
		e.cat();
		e.dog();
		e.pet();
		
		
	}

}
