package com.inh;


class Parent
{
	public void draw()
	{
		System.out.println("tle");
	}
}

class Child1 extends Parent
{
	@Override
	public void draw()
	{
		System.out.println("cle");
	}
	
}


class Child2 extends Parent
{
	@Override
	public void draw()
	{
		System.out.println("rle");
	}
}

public class Exp4 {
	
	public static void main(String[] args) {
		
		Parent obj1 = new Child1();
		
		obj1.draw();
		
		
		Parent obj2 = new Child2();
		
		obj2.draw();
	}

}
