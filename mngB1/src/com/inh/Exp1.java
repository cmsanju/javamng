package com.inh;

class A
{
	int id = 11;
	String name = "Java";
	
	public void show()
	{
		System.out.println("show()");
	}
}

class B extends A
{
	A a = new A();
	
	String city = "Blr";
	
	public void disp()
	{
		System.out.println(id+" "+name+" "+city);
	}
}

public class Exp1 {
	
	public static void main(String[] args) {
		
		B b = new B();
		
		b.show();
		b.disp();
	}

}
