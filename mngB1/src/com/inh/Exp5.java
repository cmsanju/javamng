package com.inh;

interface Inf1
{
	void disp();
	
	default void pet()
	{
		System.out.println("inf default method");
	}
	
	static void book()
	{
		System.out.println("static method");
	}
}

abstract class Abs
{
	public void show()
	{
		System.out.println("Abs normal");
	}
	
	public abstract void human();
}

class ImplTest extends Abs implements Inf1
{
	public void human()
	{
		System.out.println("abs overrided");
	}
	
	public void disp()
	{
		System.out.println("inf overrided");
	}
	
	
}

public class Exp5 {
	
	public static void main(String[] args) {
		
		ImplTest obj = new ImplTest();
		
		obj.disp();
		obj.human();
		obj.show();
		obj.pet();
		
		Inf1.book();
	}

}
