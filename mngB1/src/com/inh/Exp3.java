package com.inh;

class F
{
	public void course()
	{
		System.out.println("java");
	}
}

class G extends F
{
	public void details()
	{
		System.out.println("details()");
	}
	@Override
	public void course()
	{
		System.out.println(".net");
	}

}

class H extends F
{
	public void show()
	{
		System.out.println("show()");
	}
	
	public void course()
	{
		System.out.println("FSD JAVA");
	}
}

public class Exp3 {
	
	public static void main(String[] args) {
		
		G g = new G();
		
		g.course();
		g.details();
		g.course();
		
		H h = new H();
		
		h.course();
		h.show();
	}

}
