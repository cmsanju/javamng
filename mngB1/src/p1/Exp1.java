package p1;

public class Exp1 {
	
	private int x = 10;
	        int y = 22;
	protected int a = 27;
	public int b = 30;
	
	public void disp()
	{
		System.out.println(x);
		System.out.println(y);
		System.out.println(a);
		System.out.println(b);
	}
	
	public static void main(String[] args) {
		
		Exp1 e = new Exp1();
		
		e.disp();
	}

}

class Exp2 extends Exp1
{
	public void disp()
	{
	//	System.out.println(x); private
		System.out.println(y);
		System.out.println(a);
		System.out.println(b);
	}
}
