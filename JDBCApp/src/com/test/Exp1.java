package com.test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class Exp1 {
	
	public static void main(String[] args) throws Exception
	{
		//1 load the driver class
		Class.forName("com.mysql.jdbc.Driver");
		
		//2 create connection object
		Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/plm", "root", "password");
		
		//3 create statement object
		Statement stmt = con.createStatement();
		
		//String sql = "create table product(id int, name varchar(50), price int)";
		/*
		String sql = "insert into product values(2,'ideapad', 150)";
		
		stmt.addBatch(sql);
		
		stmt.addBatch("insert into product values(3,'asus',400)");
		
		stmt.addBatch("insert into product values(4, 'sony', 600)");
		
		//4 execute query
		stmt.executeBatch();
		*/
		
		String sql = "select * from product";
		
		ResultSet rs = stmt.executeQuery(sql);
		
		while(rs.next())
		{
			System.out.println("Product id : "+rs.getInt(1)+" Name : "+rs.getString(2)+" Price : "+rs.getInt(3));
		}
		
		//5 close the connection object
		con.close();
		
		System.out.println("Done.");
	}

}
