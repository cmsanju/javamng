package com.test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;

public class Exp2 {
	
	public static void main(String[] args) throws Exception
	{
		Class.forName("com.mysql.jdbc.Driver");
		
		Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/plm", "root","password");
		/*
		PreparedStatement pst = con.prepareStatement("insert into product values(?,?,?)");
		
		pst.setInt(1, 5);
		pst.setString(2, "Apple");
		pst.setInt(3, 5650);
		
		pst.execute();
		
		
		PreparedStatement pst = con.prepareStatement("update product set name=? where id=?");
		
		pst.setString(1, "dell");
		pst.setInt(2, 2);
		
		pst.execute();
		
		
		PreparedStatement pst = con.prepareStatement("delete from product where id =?");
		
		pst.setInt(1, 3);
		
		pst.execute();
		
		*/
		
		PreparedStatement pst = con.prepareStatement("select * from product");
		
		ResultSet rs = pst.executeQuery();
		
		ResultSetMetaData rsd = rs.getMetaData();
		
		System.out.println("Table coloumn count : "+rsd.getColumnCount());
		
		System.out.println("Name : "+rsd.getColumnName(3));
		
		while(rs.next())
		{
			System.out.println("ID : "+rs.getInt(1)+" Name : "+rs.getString(2)+" Price : "+rs.getInt(3));
		}
		
		System.out.println("Done.");
		
		con.close();
		
	}

}
