package com.test;

import java.util.List;

public class Employee {
	
	private int id;
	
	private String name;
	
	private String cmp;
	
	private List<String> skills;
	
	public Employee()
	{
		
	}
	
	public Employee(int id, String name, String cmp, List<String> skills) {
		
		this.id = id;
		this.name = name;
		this.cmp = cmp;
		this.skills = skills;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCmp() {
		return cmp;
	}

	public void setCmp(String cmp) {
		this.cmp = cmp;
	}

	public List<String> getSkills() {
		return skills;
	}

	public void setSkills(List<String> skills) {
		this.skills = skills;
	}
	
	public void disp()
	{
		System.out.println("ID : "+id+" Name : "+name+" Company : "+cmp);
		
		for(String sk : skills)
		{
			System.out.println(sk+" ");
		}
	}
}
