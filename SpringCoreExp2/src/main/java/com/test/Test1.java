package com.test;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Test1 {
	
	public static void main(String[] args) {
		
		ApplicationContext ctx = new AnnotationConfigApplicationContext(InfConfg.class);
		
		HelloWorld h = ctx.getBean("helloBean", HelloWorld.class);
		
		System.out.println(h.sayHello());
	}

}
