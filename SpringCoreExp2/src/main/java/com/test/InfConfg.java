package com.test;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class InfConfg {
	
	
	@Bean(name = "helloBean")
	public HelloWorld getObj()
	{
		return new InfImpl();
	}

}
