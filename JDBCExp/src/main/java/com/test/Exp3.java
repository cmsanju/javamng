package com.test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

public class Exp3 {
	
	public static void main(String[] args) throws Exception
	{
		
		Class.forName("com.mysql.jdbc.Driver");
		
		Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/java", "root", "password");
		
		//PreparedStatement pst = con.prepareStatement("insert into emp2 values(?,?,?)");
		
		PreparedStatement pst = con.prepareStatement("delete from emp2 where id = ?");
		
		pst.setInt(1, 101);
		//pst.setString(2, "lenovo");
		//pst.setString(3, "Hyd");
		
		pst.execute();
		
		
		
		System.out.println("Done.");
	}

}
