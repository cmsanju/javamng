package com.test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

public class Exp1 {
	
	public static void main(String[] args) throws Exception
	{
		//1 load the driver class
		Class.forName("com.mysql.jdbc.Driver");
		
		//2 create connection object
		Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/java", "root", "password");
		
		//3 create statement object
		Statement stmt = con.createStatement();
		
		//String sql = "create table emp2(id int, name varchar(50), city varchar(50))";
		
		String sql = "insert into emp2 values(1,'surodeep', 'Blr')";
		
		stmt.addBatch("update emp2 set city = 'Hyd' where id =2");
		stmt.addBatch("insert into emp2 values(4, 'Java', 'Pune')");
		
		//4 execute the query
		stmt.addBatch(sql);
		
		stmt.executeBatch();
		
		//5 close the connection object
		con.close();
		
		System.out.println("Done.");
		
	}

}
