package com.test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class Exp2 {
	
	public static void main(String[] args) throws Exception
	{
		
		Class.forName("com.mysql.jdbc.Driver");
		
		Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/java", "root", "password");
		/*
		PreparedStatement pst = con.prepareStatement("insert into emp2 values(?,?,?)");
		
		pst.setInt(1, 5);
		pst.setString(2, "apple");
		pst.setString(3, "blr");
		
		pst.execute();
		
		PreparedStatement pst = con.prepareStatement("update emp2 set name =? where id =?");
		
		pst.setString(1, "Spring");
		pst.setInt(2, 5);
		
		pst.execute();
		*/
		
		PreparedStatement pst = con.prepareStatement("select * from emp2");
		
		ResultSet rs = pst.executeQuery();
		
		while(rs.next())
		{
			System.out.println("ID : "+rs.getInt(1)+" Name : "+rs.getString(2)+" City : "+rs.getString(3));
		}
		
		System.out.println("Done.");
		
		con.close();
	}

}
