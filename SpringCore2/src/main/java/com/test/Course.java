package com.test;

public class Course {
	
	private String cname;
	private String btype;
	
	public Course()
	{
		
	}
	
	public Course(String cname, String btype)
	{
		this.cname = cname;
		this.btype = btype;
	}
	
	public String getCname() {
		return cname;
	}
	public void setCname(String cname) {
		this.cname = cname;
	}
	public String getBtype() {
		return btype;
	}
	public void setBtype(String btype) {
		this.btype = btype;
	}
	
	public void details()
	{
		System.out.println("Course Name : "+cname+" Batch Type : "+btype);
	}

}
