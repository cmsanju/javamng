package com.test;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

public class Student {
	
	private int id;
	
	private String name;
	
	private String loc;
	
	@Autowired
	private Course crs;
	
	private List<String> crs_Names;
	
	public Student()
	{
		
	}
	
	public Student(int id, String name, String loc, Course crs, List<String> crs_Names)
	{
		this.id = id;
		this.name = name;
		this.loc = loc;
		this.crs = crs;
		this.crs_Names = crs_Names;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLoc() {
		return loc;
	}

	public void setLoc(String loc) {
		this.loc = loc;
	}
	
	public Course getCrs() {
		return crs;
	}

	public void setCrs(Course crs) {
		this.crs = crs;
	}
	public List<String> getCrs_Names() {
		return crs_Names;
	}

	public void setCrs_Names(List<String> crs_Names) {
		this.crs_Names = crs_Names;
	}
	public void disp()
	{
		System.out.println("ID : "+id+" Name : "+name+" Location : "+loc);
		
		for(String nm : crs_Names)
		{
			System.out.print(nm+" ");
		}
		
		crs.details();
	}

	
}
