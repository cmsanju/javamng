package com.test;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

public class Test {
	
	public static void main(String[] args) {
		
		//Resource rs = new ClassPathResource("applicationContext.xml");
		
		//BeanFactory bn = new XmlBeanFactory(rs);
		
		ApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
		
		Student s = (Student)ctx.getBean("std");
		
		s.disp();
		
		Student s1 = (Student)ctx.getBean("std1");
		
		s1.disp();
	}

}
