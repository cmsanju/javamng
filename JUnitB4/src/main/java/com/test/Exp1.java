package com.test;

public class Exp1 {
	
	public int add(int x, int y)
	{
		return x+y;
	}
	
	public int mul(int x, int y)
	{
		return x*y;
	}
	
	public int sub(int x, int y)
	{
		return x-y;
	}
	
	public String greetPpl(String msg)
	{
		return msg;
	}
}
