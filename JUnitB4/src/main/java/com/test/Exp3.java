package com.test;

import java.util.Stack;

public class Exp3 {
	
	public static void main(String[] args) {
		
		Stack data = new Stack();
		
		data.add("java");
		data.add(10);
		data.add(22.33f);
		data.add(45.87);
		data.add('C');
		data.add(10);
		data.add("java");
		data.add("hello");
		
		System.out.println(data);
		
		System.out.println(data.peek());
		
		data.push("mac");
		
		System.out.println(data.pop());
		
		System.out.println(data.empty());
		
		System.out.println(data.search(100));
	}

}
