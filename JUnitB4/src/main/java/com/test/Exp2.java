package com.test;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class Exp2 {
	
	public static void main(String[] args) {
		
		//List data = new ArrayList();
		
		List data = new LinkedList();
		
		data.add("java");
		data.add(10);
		data.add(22.33f);
		data.add(45.87);
		data.add('C');
		data.add(10);
		data.add("java");
		
		System.out.println(data);
		
		Iterator itr = data.iterator();
		
		while(itr.hasNext())
		{
			System.out.println(itr.next());
		}
		
		System.out.println(data.contains(10));
		
		System.out.println(data.size());
	}

}
