package com.test;

import java.util.TreeSet;

public class Exp5 {
	
	public static void main(String[] args) {
		
		TreeSet<Integer> data = new TreeSet<Integer>();
		
		data.add(33);
		data.add(11);
		data.add(5);
		data.add(4);
		data.add(35);
		data.add(10);
		data.add(7);
		
		System.out.println(data);
	}

}
