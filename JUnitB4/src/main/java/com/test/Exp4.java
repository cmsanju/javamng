package com.test;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.Set;

public class Exp4 {
	
	public static void main(String[] args) {
		
		Set data = new HashSet();
		
		data.add("java");
		data.add(10);
		data.add(22.33f);
		data.add(45.87);
		data.add('C');
		data.add(10);
		data.add("java");
		
		System.out.println(data);
		
		
		Set dat = new LinkedHashSet();
		
		dat.add("java");
		dat.add(10);
		dat.add(22.33f);
		dat.add(45.87);
		dat.add('C');
		dat.add(10);
		dat.add("java");
		
		System.out.println(dat);
	}

}
