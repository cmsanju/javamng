package com.test;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;



public class StoreData {
	
	public static void main(String[] args) {
		
		Configuration cfg = new Configuration();
		
		cfg.configure("hibernate.cfg.xml");
		
		SessionFactory sf = cfg.buildSessionFactory();
		
		Session session = sf.openSession();
		
		Transaction t = session.beginTransaction();
		
		Customer cst = new Customer();
		
		cst.setCust_name("Hibernate");
		cst.setCust_type("Regular");
		
		session.persist(cst);
		
		t.commit();
		
		session.close();
		
		System.out.println("Done.");
	}

}
