package com.test;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class StoreData {
	
	public static void main(String[] args) {
		
		Configuration cfg = new Configuration();
		
		cfg.configure("hibernate.cfg.xml");
		
		SessionFactory sf = cfg.buildSessionFactory();
		
		Session session = sf.openSession();
		
		Transaction t = session.beginTransaction();
		
		Course crs = new Course();
		
		crs.setC_name("Java");
		
		Course crs1 = new Course();
		
		crs.setC_name("FSD JAVA");
		
		List<Course> lcrs = new ArrayList<Course>();
		
		lcrs.add(crs);
		lcrs.add(crs1);
		
		Student std = new Student();
		
		std.setS_name("Ranjan");
		

		session.persist(std);
		
		
		t.commit();
		
		//session.close();
		
		System.out.println("Done.");
		
	}

}

/*
 * one TO one relation (matching records the two tables there is no list type data)
 * ManytoOne(Left side table all the records right matching data)
 * ManyToMany(all the records from left side and all the records from right side )
 * OneToMany(left side only matching records and right side all the records )
 * 
 */
