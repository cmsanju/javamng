<%@page import="java.util.Date"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
		<h1>Simple JSP Code.</h1>
		
		<!-- Declaration tag -->
		
		<%!
		
			int x = 50;
			int y = 500;
			
			public int add()
			{
				return x+y;
			}
		
		%>
		
		<!-- expression tag -->
		
		<%= add()%>
		
		<!-- scriplet tag -->
		
		<%
			Date date = new Date();
			
			out.println("Current Date : "+date);
		%>
		
</body>
</html>