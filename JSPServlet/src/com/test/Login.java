package com.test;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.Statement;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/Login")
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		try {
		
		response.setContentType("text/html");
		
		PrintWriter out = response.getWriter();
		
		String name = request.getParameter("user");
		String pass = request.getParameter("pwd");
		
		User usr = new User();
		
		usr.setUserName(name);
		usr.setPassword(pass);
		
		// load the driver class
		
		Class.forName("com.mysql.jdbc.Driver");
		
		//create the connection object
		
		Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/evngb1", "root", "password");
		
		//create statement object
		
		//Statement stmt = con.createStatement();
		
		//String sql = "insert into user905 values(111,'"+usr.getUserName()+"', '"+usr.getPassword()+"')";
		
		PreparedStatement pst = con.prepareStatement("insert into user905 values(?,?,?)");
		
		pst.setInt(1, 10);
		pst.setString(2, usr.getUserName());
		pst.setString(3, usr.getPassword());
		
		pst.execute();
		
		out.println("Data stored");
		
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
}
