package com.test;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Test {
	
	public static void main(String[] args) {
		
		ApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
		
		EmpDao e = ctx.getBean("edao", EmpDao.class);
		
		Employee emp = new Employee();
		
		emp.setId(102);
		emp.setName("Surodeep");
		emp.setCmp("Dell");
		
		//e.save(emp);
		
		e.update(emp);
		
		System.out.println("Done.");
	}

}
