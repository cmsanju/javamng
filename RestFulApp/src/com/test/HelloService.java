package com.test;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/hello")
public class HelloService 
{
	@GET
	@Path("/text")
	@Produces(MediaType.TEXT_PLAIN)
	public String sayHelloPalinText()
	{
		return "HelloTest plain text service";
	}
	
	@GET
	@Path("/html/{name}")
	@Produces(MediaType.TEXT_HTML)
	public String sayHelloHtmlText(@PathParam("name") String user)
	{
		return"<html><body><h1>Hi this is HTML SERVICE "+user+"</h1></body></html>";
	}
}
/*

@GET IS FETCH THE DATA  SELECT
@POST CREATE THE RESOURCE  INSERT
@PUT UPDATE THE RESOURCE  UPDATE
@DELETE DELETE THE RECORD DELETE

*/