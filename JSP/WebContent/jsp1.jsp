<%@page import="java.util.Date"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
		<h1>JSP Example1</h1>
		
		<!-- declaration tag -->
		
		<%!
			
			public int add()
			{
			  int x = 40;
			  int y = 60;
			  
			  return x+y;
			}
		
		%>
		
		<!-- expression tag -->
		
		<%= add() %>
		
		<!-- scriplet tag -->
		
		<%
		
				Date date = new Date();
		
		 		out.println("<br>"+date);
		
		%>
		
</body>
</html>