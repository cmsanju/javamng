package com.test;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Test {
	
	public static void main(String[] args) {
		
		ApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
		
		//Address adr = (Address)ctx.getBean("adr");
		
		Employee emp = (Employee)ctx.getBean("emp");
		
		Employee emp1 = (Employee)ctx.getBean("emp1");
		
		//adr.details();
		emp.disp();
		emp1.disp();
		
		
		
	}

}
