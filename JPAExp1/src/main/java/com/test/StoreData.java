package com.test;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class StoreData {
	
	public static void main(String[] args) {
		
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("Employee_Details");
		
		EntityManager em = emf.createEntityManager();
		
		em.getTransaction().begin();
		
		Employee emp = new Employee();//transient
		
		emp.setEmp_id(201);
		emp.setEmp_name("java");
		emp.setEmp_city("Blr");
		
		//em.persist(emp);//persistence 
		
		Employee emp1 = em.find(Employee.class, 201);
		
		System.out.println(emp1.getEmp_id()+" "+emp1.getEmp_name()+" "+emp1.getEmp_city());
		
		//emp1.setEmp_name("Hibernate");
		
		em.remove(emp1);
		
		System.out.println(emp1.getEmp_id()+" "+emp1.getEmp_name()+" "+emp1.getEmp_city());
		
		em.getTransaction().commit();
		
		System.out.println("Done.");
		
		emf.close();
		em.close();//detached 
	}

}
