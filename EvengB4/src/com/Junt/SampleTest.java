package com.Junt;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class SampleTest {
	
	Sample obj;
	
	@BeforeAll
	public static void beforeAll()
	{
		System.out.println("before all the test cases");
	}
	
	@AfterAll
	public static void afterAll()
	{
		System.out.println("after all the test cases");
	}
	
	@BeforeEach
	public void setUp()
	{
		System.out.println("before test method");
		
		obj = new Sample();
	}
	@AfterEach
	public void setDown()
	{
		System.out.println("after test method");
		
		obj = null;
	}
	
	@Test
	public void testAdd()
	{
		System.out.println("test add method");
		
		int x = obj.add(10, 30);
		
		assertEquals(40, x);
	}
	@Test
	public void testSub()
	{
		System.out.println("test sub method");
		
		assertEquals(25, obj.sub(40, 15));
	}
	@Test
	public void testMul()
	{
		System.out.println("test mul method");
		
		assertEquals(80, obj.mul(40, 2));
	}
	@Test
	public void testGreet()
	{
		System.out.println("test greet method");
		
		assertEquals("hi hello", obj.greet("hi hello"));
	}
}
