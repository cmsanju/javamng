package com.fls;

import java.io.BufferedReader;
import java.io.FileReader;

public class ReadData {
	
	public static void main(String[] args) throws Exception
	{
		
		FileReader fr = new FileReader("src/sample.txt");
		
		BufferedReader br = new BufferedReader(fr);
		
		System.out.println(br.readLine());
	}

}
