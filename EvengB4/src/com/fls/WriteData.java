package com.fls;

import java.io.FileWriter;

public class WriteData {
	
	public static void main(String[] args) throws Exception
	{
		
		FileWriter fw = new FileWriter("src/sample.txt");
		
		String msg = "this is file write operation using char stream";
		
		fw.write(msg);
		
		fw.flush();
		
		System.out.println("Done.");
	}

}
