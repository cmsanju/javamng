package com.fls;

import java.io.FileOutputStream;
import java.io.ObjectOutputStream;

public class SerialDemo {
	
	public static void main(String[] args) throws Exception
	{
		
		FileOutputStream fos = new FileOutputStream("src/employee.txt");
		
		ObjectOutputStream ops = new ObjectOutputStream(fos);
		
		Employee emp = new Employee();
		
		emp.id = 111;
		emp.name = "Java";
		emp.cmp = "Dell";
		emp.city = "Blr";
		emp.pin = 123123;
		
		ops.writeObject(emp);
		
		System.out.println("Done.");
	}

}
