package com.cls;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map.Entry;

public class Exp1 {
	
	public static void main(String[] args) {
		
		HashMap<String, Integer> data = new HashMap<String, Integer>();
		
		data.put("lenovo", 234);
		data.put("dell", 453);
		data.put("asus", 484);
		data.put("sony", 223);
		data.put("asus", 3737);
		data.put("apple", 3838);
		data.put("mac", 323);
		
		System.out.println(data);
		
		LinkedHashMap<String, Integer> dat =  new LinkedHashMap<String, Integer>();
		
		dat.put("lenovo", 234);
		dat.put("dell", 453);
		dat.put("asus", 484);
		dat.put("sony", 223);
		dat.put("asus", 3737);
		dat.put("apple", 3838);
		dat.put("mac", 323);
		
		System.out.println(dat);
		
		
		Iterator<Entry<String, Integer>> itr = dat.entrySet().iterator();
		
		while(itr.hasNext())
		{
			Entry<String, Integer> et = itr.next();
			
			System.out.println(et.getKey()+" "+et.getValue());
		}
		
		for(String ky : data.keySet())
		{
			System.out.println(ky+" "+data.get(ky));
		}
	}

}
