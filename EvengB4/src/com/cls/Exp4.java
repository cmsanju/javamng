package com.cls;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

class Student implements Comparable<Student>//,Comparator<Student>
{
	private int id;
	private String name;
	private int age;
	

	@Override
	public int compareTo(Student o) {
		return this.id - o.id;
	}
	
	public Student()
	{
		
	}
	
	public Student(int id, String name, int age)
	{
		this.id = id;
		this.name = name;
		this.age = age;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}
}

class NameComparator implements Comparator<Student>
{

	@Override
	public int compare(Student o1, Student o2) {
		
		return o1.getName().compareTo(o2.getName());
	}
	
}

public class Exp4 {
	
	public static void main(String[] args) {
		
		List<Student> data = new ArrayList<Student>();
		
		data.add(new Student(12, "Java", 12));
		data.add(new Student(5, "Sql", 9));
		data.add(new Student(20, "Spring", 6 ));
		data.add(new Student(3, "Hibernate", 4));
		
		Collections.sort(data, new NameComparator());
		
		for(Student dt  :data)
		{
			System.out.println(dt.getId()+" "+dt.getName()+" "+dt.getAge());
		}
		
	}

}
