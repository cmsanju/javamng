package com.cls;

import java.util.HashMap;

public class Exp3 {
	
	public static void main(String[] args) {
		
		HashMap<Customer, Integer> data = new HashMap<Customer, Integer>();
		
		data.put(new Customer(111, "Java", "Blr"), 1);
		
		data.put(new Customer(222, "Java", "Blr"), 2);
		
		data.put(new Customer(333, "Java", "Blr"), 3);
		
		data.put(new Customer(444, "Java", "Blr"), 4);
		
		data.put(new Customer(555, "Java", "Blr"), 5);
		
		System.out.println(data);
		
		for(Customer ct : data.keySet())
		{
			System.out.println(ct.getId()+" "+ct.getName()+" "+ct.getCity());
		}
		
	}

}
