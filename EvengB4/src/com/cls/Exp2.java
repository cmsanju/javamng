package com.cls;

import java.util.TreeMap;

public class Exp2 {
	
	public static void main(String[] args) {
		
		TreeMap<String, Integer> data = new TreeMap<>();
		
		data.put("lenovo", 234);
		data.put("dell", 453);
		data.put("asus", 484);
		data.put("sony", 223);
		data.put("asus", 3737);
		data.put("apple", 3838);
		data.put("mac", 323);
		
		System.out.println(data);
		
		for(String ky : data.keySet())
		{
			System.out.println("Product : "+ky+" Price : "+data.get(ky));
		}
	}

}
