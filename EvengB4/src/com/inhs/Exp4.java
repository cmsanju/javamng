package com.inhs;

interface J
{
	float pi = 3.14f;
	
	void pet();
	
	default void animal()
	{
		System.out.println("default");
	}
	
	static void dog()
	{
		System.out.println("static");
	}
}

abstract class K
{
	public abstract void book();
	
	public void pens()
	{
		System.out.println("abs pens");
	}
	
}

class Impl extends K implements J
{

	@Override
	public void book() {
		
		System.out.println("abs overrided");
	}

	@Override
	public void pet() {
		
		System.out.println("INF overrided");
	}
	
}

public class Exp4 {
	
	public static void main(String[] args) {
		
		Impl obj = new Impl();
		
		obj.animal();
		obj.book();
		obj.pens();
		obj.pet();
		
		J.dog();
		
	}

}
