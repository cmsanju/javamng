package com.inhs;

class C
{
	public void human()
	{
		System.out.println("Top parent");
	}
}

class D extends C
{
	public void animal()
	{
		System.out.println("INTERM CLASS");
	}
}

class E extends D
{
	public void pet()
	{
		System.out.println("btm child class");
	}
}

public class Exp2 {
	
	public static void main(String[] args) {
		
		E e = new E();
		
		e.human();
		e.animal();
		e.pet();
	}

}
