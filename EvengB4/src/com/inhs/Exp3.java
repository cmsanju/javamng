package com.inhs;

class F
{
	public void book()
	{
		System.out.println("English");
	}
}

class G extends F
{
	@Override
	public void book()
	{
		System.out.println("Hindi");
	}
}

class H extends F
{
	@Override
	public void book()
	{
		System.out.println("ET");
	}
}

public class Exp3 {
	
	public static void main(String[] args) {
		G g = new G();
		
		g.book();
		
		H h = new H();
		
		h.book();
		
		
		F f1 = new G();
		
		f1.book();
		
		F f2 = new H();
		
		f2.book();
	}

}
