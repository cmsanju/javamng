package com.inhs;

class A
{
	int pin = 3434;
	String name = "java";
	
	public void disp()
	{
		System.out.println("parent");
	}
}

class B extends A
{
	String city = "Blr";
	
	public void details()
	{
		System.out.println(pin+" "+name+" "+city);
	}
}

public class Exp1 {
	
	public static void main(String[] args) {
		
		B b = new B();
		
		b.details();
	}

}
