package com.inhs;

@FunctionalInterface
interface FunInf
{
	void greet();
	
}

public class Exp7 {
	
	public static void main(String[] args) {
		
		FunInf obj = new FunInf()
				{
				 @Override
			     public void greet()
			     {
			    	 System.out.println("overrided");
			     }
			
				};
				
				obj.greet();
				
				//jdk 1.8 feature 
				
		FunInf obj1 = () -> System.out.println("lambda expression");
		
		obj1.greet();
	}

}
