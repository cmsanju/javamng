package com.excp;

public class Exp1 {
	
	public static void main(String[] args) {
		
		try
		{
			System.out.println(330/3);
			
			System.out.println(13/0);
			
			int[] ar = {12,34,56,78};
			
			System.out.println(ar[1]);
			
			String str = null;
			
			System.out.println(str.charAt(5));
			
			
		}
		catch(ArithmeticException ae )
		{
			System.out.println("can't divided by zero");
		}
		catch(ArrayIndexOutOfBoundsException aie)
		{
			System.out.println("check your array size");
		}
		catch(NullPointerException npe)
		{
			System.out.println("enter string value");
		}
		catch(StringIndexOutOfBoundsException sie)
		{
			System.out.println("check string length");
		}
		catch(Exception e)
		{
			System.out.println("check your inputs");
		}
		finally
		{
			System.out.println("i am from finally.");
		}
	}

}
