package com.test;

public class Exp1 {
	
	public static void main(String[] args) {
		
		String str1 = "java";
		String str2 = "hi";
		String str3 = "java";
		
		String str4 = new String("java");
		String str5 = new String("hi");
		
		String str6 = new String("java");
		
		System.out.println(str1 == str3);
		
		System.out.println(str2 == str5);
		
		System.out.println(str4 == str6);
		
		System.out.println(str4.equals(str6));
		
		 str1.concat(" 1.8");
		
		System.out.println(str1);
		
		StringBuffer sb = new StringBuffer(str1);
		
		sb.append(" 1.8");
		
		System.out.println(sb);
	}

}
