package com.test;

import java.util.regex.MatchResult;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Exp5 {
	
	public static void main(String[] args) {
		
		Pattern pt = Pattern.compile("..a");
		Matcher mt = pt.matcher("aaa");
		
		System.out.println(mt.matches());
		
		System.out.println(Pattern.matches("..j", "aaj"));
		
		System.out.println(Pattern.matches("[abc]+", "aa"));
		
		System.out.println(Pattern.matches("\\d", "1"));
		
		System.out.println(Pattern.matches("[a-zA-Z0-9]{8}", "java1232"));
		
		System.out.println(Pattern.matches("[89][0-9]{9}", "9853452784"));
	}

}
