package com.test;

public class Exp4 {
	
	@Override
	protected void finalize()
	{
		System.out.println("object destroyed");
	}
	
	public void disp()
	{
		
	}
	
	public static void main(String[] args) {
		
		String val1 = "300";
		String val2 = "500";
		
		System.out.println(val1+val2);
		
		
		int x = Integer.parseInt(val1);
		int y = Integer.parseInt(val2);
		
		System.out.println(x+y);
		
		double d1 = Double.parseDouble(val1);
		double d2 = Double.parseDouble(val2);
		
		System.out.println(d1+d2);
		
		Exp4 obj = new Exp4();
		
		System.out.println(obj.hashCode());
		
		System.out.println(val2.hashCode());
		
		obj = null;
		
		//obj.disp();
		
		System.gc();
				
	}

}
