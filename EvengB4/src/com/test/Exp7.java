package com.test;

public class Exp7 {
	
	public static void main(String[] args) {
		
		int[] ar = {12,34,56,78,90};
		
		for(int i = 0; i < ar.length; i++)
		{
			System.out.println(ar[i]);
		}
		
		
		for(int x : ar)
		{
			System.out.println(x);
		}
		
		int[][] ar1 = {{12,23,1}, {34,45,2}, {56,67,3}};
		
		System.out.println(ar1[0][0]);
		
		for(int i = 0; i < ar1.length; i++)
		{
			for(int j = 0; j < ar1[i].length; j++)
			{
				System.out.print(" "+ar1[i][j]);
			}
			System.out.println();
		}
		
		for (int[] x : ar1) {
			for(int y : x)
			{
				System.out.print(" "+y);
			}
			System.out.println();
		}
	}

}
