package com.test;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Exp6 {
	
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		System.out.println("enter pass word");
		
		String pass = sc.next();
		
		//pattern
		
		String ptr = "^.*(?=.{8,})(?=..*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%&+=]).*$";
		
		Pattern pt = Pattern.compile(ptr);
		
		Matcher mt = pt.matcher(pass);
		
		if(mt.matches())
		{
			System.out.println("valid password");
		}
		else
		{
			System.out.println("invalid password");
		}
		
	}

}
