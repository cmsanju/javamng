package p2;

class Student
{
	static int x;
	 int pin;
	public Student()
	{
		this(10);
		System.out.println("default");
	}
	
	public Student(int x)
	{
		this("hello");
		System.out.println("single arg");
	}
	
	public Student(String msg)
	{
		this(20,"hi");
		System.out.println("string arg");
	}
	
	public Student(int x, String msg)
	{
		System.out.println("double args");
	}
	
	static
	{
		System.out.println("static block");
		
		x = 300;
	}
	
	{
		System.out.println("instance block");
		
		pin = 1234;
	}
}

public class Exp6 {
	
	public static void main(String[] args) {
		
		Student obj1 = new Student();
		
		Student obj2 = new Student();
		
	}

}
