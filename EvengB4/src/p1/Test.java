package p1;

class Employee
{
	Employee obj = null;
	//1 default constructor
	
	public Employee()
	{
		System.out.println("default");
	}
	
	//2 parameterised constructors
	
	public Employee(int x, int y)
	{
		System.out.println("parameterised");
	}
	
	//3 overloaded constructor
	
	public Employee(String msg)
	{
		System.out.println("over loaded");
	}
	
	//4 object parameterised constructor
	
	public Employee(Employee obj)
	{
		System.out.println("object parameterised");
		
		this.obj = obj;	
	}
	
	//factory method is nothing but method return statement is same name as the class name
	
	public Employee getObj()
	{
		return obj;
	}
}

public class Test {
	
	public static void main(String[] args) {
		
		Employee obj = new Employee();
		
		Employee obj1 = new Employee(39,50);
		
		Employee obj2 = new Employee("Java");
		
		Employee obj3 = new Employee(obj);	
		
		Employee obj4 = obj.getObj();
	}

}
