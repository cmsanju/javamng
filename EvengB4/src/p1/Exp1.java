package p1;

public class Exp1 {
	
	private int j = 30;
	
	  int k = 40;
	       
	protected int l = 50;
	
	public int m = 90;
	
	
	public void show()
	{
		System.out.println(j);
		System.out.println(k);
		System.out.println(l);
		System.out.println(m);
	}
	
	public void method1(int x, int y)
	{
		System.out.println(x+y);
	}
	
	public static void main(String[] args) {
		
		Exp1 e = new Exp1();
		
		e.show();
		e.method1(10, 10);
	}
	

}

class Exp2 extends Exp1
{
	public void show()
	{
		//System.out.println(j); private
		System.out.println(k);
		System.out.println(l);
		System.out.println(m);
	}
}
