package com.cls;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

class Employee implements Comparable<Employee>
{
	private int id;
	
	private String name;
	
	private String city;
	
	public int compareTo(Employee emp)
	{
		return this.id-emp.id;
	}
	
	public Employee()
	{
		
	}
	
	public Employee(int id, String name, String city)
	{
		this.id = id;
		this.name = name;
		this.city = city;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}
}

class NameComparator implements Comparator<Employee>
{
	public int compare(Employee e1, Employee e2)
	{
		return e1.getName().compareTo(e2.getName());
	}
}

public class Exp5 {
	
	public static void main(String[] args) {
		
		List<Employee> empList = new ArrayList<Employee>();
		
		empList.add(new Employee(11, "Java", "blr"));
		
		empList.add(new Employee(2, "spring", "hyd"));
		
		empList.add(new Employee(4, "Hibernate", "pune"));
		
		empList.add(new Employee(9, "java", "kolkata"));
		
		empList.add(new Employee(5, "hello", "user"));
		
		//Collections.sort(empList);
		
		Collections.sort(empList, new NameComparator());
		
		for(Employee obj : empList)
		{
			System.out.println(obj.getId()+" "+obj.getName()+" "+obj.getCity());
		}
		
		//CRUD
		
		empList.set(4, new Employee(6, "Hero", "Blr"));
		
		for(int i = 0; i<empList.size(); i++) {
		if(i == 4)
		empList.remove(i);
		}
	}

}
