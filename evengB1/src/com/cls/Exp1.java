package com.cls;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class Exp1 {
	
	public static void main(String[] args) {
		
		List list = new LinkedList();
		
		list.add(12);
		list.add("java");
		list.add(false);
		list.add(2.3f);
		list.add("java");
		list.add(34.99);
		list.add('A');
		
		System.out.println(list);
		
		System.out.println(list.size());
		
		//Iterator
		
		Iterator itr = list.iterator();
		
		while(itr.hasNext())
		{
			System.out.println(itr.next());
		}
		
		boolean b = list.contains("java");
		
		System.out.println(b);
		
	}

}
