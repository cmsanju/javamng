package com.cls1;

import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;

public class Exp2 {
	
	public static void main(String[] args) {
		
		//Set data = new HashSet();
		
		Set data = new LinkedHashSet();
		
		data.add(10);
		data.add("java");
		data.add('A');
		data.add(34.56);
		data.add(10);
		data.add("java");
		data.add(false);
		data.add(34.88f);
		
		System.out.println(data);
		
		Iterator itr = data.iterator();
		
		while(itr.hasNext())
		{
			System.out.println(itr.next());
		}
	}

}
