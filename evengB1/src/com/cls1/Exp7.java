package com.cls1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

class Employee implements Comparable<Employee>
{
	private int id;
	private String name;
	private String city;
	
	public int compareTo(Employee e)
	{
		return this.id-e.id;
	}
	
	public Employee()
	{
		
	}
	
	public Employee(int id, String name, String city)
	{
		this.id = id;
		this.name = name;
		this.city = city;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
}

class NameComparator implements Comparator<Employee>
{
	public int compare(Employee e1, Employee e2)
	{
		return e2.getName().compareTo(e1.getName());
	}
}

public class Exp7 {
	
	public static void main(String[] args) {
		
		List<Employee> empList = new ArrayList<Employee>();
		
		empList.add(new Employee(10,"zero","Blr"));
		
		Employee e = new Employee();
		
		e.setId(2);
		e.setName("java");
		e.setCity("hYD");
		
		empList.add(e);
		empList.add(new Employee(1,"banana","Blr"));
		empList.add(new Employee(4,"apple","Blr"));
		empList.add(new Employee(3,"orange","Blr"));
		
		Collections.sort(empList,new NameComparator());
		
		for(Employee emp : empList)
		{
			System.out.println(emp.getId()+" "+emp.getName()+" "+emp.getCity());
		}
		
	}

}
