package com.cls1;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

public class Exp5 {
	
	public static void main(String[] args) {
		
		//Map<String, Integer> data = new HashMap<String, Integer>();
		
		//Map<String, Integer> data = new LinkedHashMap<String, Integer>();
		
		Map<String, Integer> data = new Hashtable<String, Integer>();
		
		data.put("lenovo", 3000);
		data.put("dell", 420);
		data.put("asus", 350);
		data.put("sony", 4000);
		data.put("mac", 5000);
		data.put("dell", 500);
		
		System.out.println(data);
		
		Iterator<Entry<String, Integer>> itr = data.entrySet().iterator();
		
		while(itr.hasNext())
		{
			Entry<String, Integer> et = itr.next();
			
			System.out.println("Key : "+et.getKey()+" Value : "+et.getValue());
		}
		
		for(String ky : data.keySet())
		{
			System.out.println("Key : "+ky+" Value : "+data.get(ky));
		}
		
		for(Integer v : data.values())
		{
			System.out.println(v);
		}
	}

}
