package com.cls1;

import java.util.Stack;

public class Exp4 {
	
	public static void main(String[] args) {
		
		Stack data = new Stack();
		
		data.add(10);
		data.add("java");
		data.add('A');
		data.add(34.56);
		data.add(10);
		data.add("java");
		data.add(false);
		data.add(34.88f);
		
		System.out.println(data);
		
		System.out.println(data.peek());
		System.out.println(data.pop());
		
		
		
		data.push(222);
		
		System.out.println(data);
		
		System.out.println(data.search(100));
		
		data.clear();
		
		System.out.println(data.empty());
	}

}
