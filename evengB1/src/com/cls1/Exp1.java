package com.cls1;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

public class Exp1 {
	
	public static void main(String[] args) {
		
		//List data = new ArrayList();
		
		List data = new LinkedList();
		
		data.add(10);
		data.add("java");
		data.add('A');
		data.add(34.56);
		data.add(10);
		data.add("java");
		data.add(false);
		data.add(34.88f);
		
		System.out.println(data);
		
		System.out.println(data.size());
		
		//Iterator itr = data.iterator();
		ListIterator itr = data.listIterator();
		
		while(itr.hasNext())
		{
			System.out.println(itr.next());
		}
		
		System.out.println("=========");
		
		while(itr.hasPrevious())
		{
			System.out.println(itr.previous());
		}
		
		data.set(6, "Spring");
		
		System.out.println(data);
		
		data.remove(5);
		
		System.out.println(data);
		
		//CRUD
	}

}
