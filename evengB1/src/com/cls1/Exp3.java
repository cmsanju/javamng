package com.cls1;

import java.util.TreeSet;

public class Exp3 {
	
	public static void main(String[] args) {
		
		TreeSet<Integer> data = new TreeSet<>();
		
		data.add(10);
		data.add(2);
		data.add(20);
		data.add(3);
		data.add(1);
		data.add(5);
		data.add(4);
		data.add(9);
		
		System.out.println(data);
		
		
		TreeSet<String> dat = new TreeSet<>();
		
		dat.add("xerox");
		dat.add("zero");
		dat.add("java");
		dat.add("hello");
		dat.add("apple");
		dat.add("asus");
		dat.add("lenovo");
		
		System.out.println(dat);
		
		data.forEach(v -> System.out.println(v));
	}

}
