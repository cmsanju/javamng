package com.cls1;

import java.util.Iterator;
import java.util.Map.Entry;
import java.util.TreeMap;

public class Exp6 {
	
	public static void main(String[] args) {
		
		TreeMap<String, Integer> data = new TreeMap<String, Integer>();
		
		data.put("lenovo", 3000);
		data.put("dell", 420);
		data.put("asus", 350);
		data.put("sony", 4000);
		data.put("mac", 5000);
		data.put("dell", 500);
		data.put("apple", 403);
		//data.put(null, null);
		
		System.out.println(data);
		
		Iterator<Entry<String, Integer>> itr = data.entrySet().iterator();
		
		while(itr.hasNext())
		{
			Entry<String,Integer> et = itr.next();
			
			System.out.println("Key : "+et.getKey()+" Value : "+et.getValue());
		}
		
		data.forEach((k,v)->System.out.println("Product : "+k+" Price : "+v));
		
		
	}

}
