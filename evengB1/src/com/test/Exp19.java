package com.test;

public class Exp19 {
	
	public static void main(String[] args) {
		
		try {
			
			System.out.println("test");
		
			System.out.println(33/1);
		
			System.out.println("hello");
		
			int[] ar = {12,33,44,55};
		
			System.out.println(ar[0]);
		
			String str = "java";
		
			System.out.println(str.charAt(2));
		
			String name = "hello";
		
			System.out.println(name.charAt(0));
			
			String str1 = "aa";
			
			int x = Integer.parseInt(str1);
		}
		catch(ArithmeticException ae)
		{
			System.out.println("can't divided by zero");
		}
		catch(ArrayIndexOutOfBoundsException aie)
		{
			System.out.println("check your array size");
			//System.exit(0);
		}
		catch(StringIndexOutOfBoundsException sie)
		{
			System.out.println("check your string length");
		}
		catch(NullPointerException npe)
		{
			System.out.println("pleas enter name");
		}
		catch(Exception e)
		{
			System.out.println("check your inputs");
		}
		finally
		{
			System.out.println("i am from finally");
		}
	}

}
/*

1 try block
2 catch block
3 finally block
4 throws
5 throw

*/