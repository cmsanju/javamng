package com.test;

class Hello7373
{
	
}

public class Exp5 {
	
public static void main(String[] args) {
		
		//method1(10);
		
		Exp5 obj = new Exp5();
		
		obj.method1(20);
		obj.method2();
		obj.method3("Java");
		obj.method4();
		
		System.out.println(obj.method4());
		System.out.println(obj.method1(300));
		
	}
	
	//type 1 method taking the arguments and returning the value
	
	public int method1(int x)
	{
		System.out.println("method 1");
		
		return 66;
	}
	
	//type 2 method not taking the arguments and not returning
	
	public void method2()
	{
		System.out.println("method 2");
	}
	
	//type 3 method taking the arguments but not returning the value
	
	public void method3(String str)
	{
		System.out.println("method 3");
	}
	
	//type 4 method not taking the arguments but returning the value
	
	public String method4()
	{
		System.out.println("method 4");
		
		return "hello";
	}
	
	

}
