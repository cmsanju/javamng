package com.test;

public class Exp11 {
	
	public static void main(String[] args) {
		
		System.out.println("Test");
		
		String val1 = "200";
		String val2 = "300";
		
		System.out.println(val1+val2);
		
		int x = Integer.parseInt(val1);
		int y = Integer.parseInt(val2);
		
		System.out.println(x+y);
		
		double d1 = Double.parseDouble(val1);
		double d2 = Double.parseDouble(val2);
		
		System.out.println(d1+d2);
				
	}

}
