package com.test;

class Singleton
{
	private static  Singleton obj=null;
	
	private Singleton()
	{
		
	}
	
	public static Singleton creatObj()
	{
		if(obj == null)
		{
			obj = new Singleton();
		}
		
		return obj;
	}
}

public class SingletonDemo {
	
	public static void main(String[] args) {
		
		
		Singleton.creatObj();	
		
	}
}
