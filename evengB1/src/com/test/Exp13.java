package com.test;

public class Exp13 {
	
	public static void main(String[] args) {
		
		String str1 = "java";
		
		String str2 = "java";
		
		String str3 = new String("java");
		String str4 = new String("java");
		
		System.out.println(str1 == str2);
		
		System.out.println(str1 == str4);
		
		System.out.println(str3 == str4);
		
		System.out.println(str1.equals(str4));
		
		
		StringBuffer sb = new StringBuffer(str1);
		
		System.out.println(sb.reverse());
		
		sb.append(" 1.8");
		
		System.out.println(sb);
	}

}
