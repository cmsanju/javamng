package com.test;

public class Exp15 {
	
	public static void main(String[] args) {
		
		int[] ar = {10,20,30,40,50,60,70};
		
		System.out.println(ar[0]);
		
		for(int i = 0; i < ar.length; i++)
		{
			System.out.print(ar[i]+" ");
		}
		
		for(int x : ar)
		{
			System.out.println(x);
		}
		
		int[] ar1 = new int[5];
		
		ar1[0] = 20;
		ar1[1] = 30;
		ar1[2] = 40;
		ar1[3] = 50;
		ar1[4] = 60;
		//ar1[5] = 70;
		
		System.out.println(ar1[0]);
	}

}
