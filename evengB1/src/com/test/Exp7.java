package com.test;

import java.util.Scanner;

class Order
{
	private int id;
	private String name;
	private String type;
	private double price;
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
}


public class Exp7 {
	
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		System.out.println("enter id");
		
		int id = sc.nextInt();
		
		System.out.println("enter name");
		
		String name = sc.next();
		
		System.out.println("enter type");
		
		String type = sc.next();
		
		System.out.println("enter price");
		
		double price = sc.nextDouble();
		
		Order obj = new Order();
		
		obj.setId(id);
		obj.setName(name);
		obj.setType(type);
		obj.setPrice(price);
		
		System.out.println("ID : "+obj.getId()+" Name : "+obj.getName()+" Type : "+obj.getType()+" Price : "+obj.getPrice());
		
		
	}

}
