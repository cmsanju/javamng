package com.test;

class Employee
{
	//default constructor
	public Employee()
	{
		System.out.println("default");
	}
	
	//parameterised constructor 
	public Employee(int x, String name)
	{
		System.out.println("parameterised");
	}
	
	//overloaded constructor
	public Employee(double d)
	{
		System.out.println("overloaded");
	}
	//object parameterised constructor
	public Employee(Employee obj)
	{
		System.out.println("object parameterised");
	}
}

public class Exp8 {
	
	public static void main(String[] args) {
		
		Employee e1 = new Employee();
		Employee e2 = new Employee(44, "Java");
		Employee e3 = new Employee(33.44);
		Employee e4 = new Employee(e1);
	}

}
