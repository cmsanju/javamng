package com.test;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Exp17 {
	
	public static void main(String[] args) {
		
		Pattern p = Pattern.compile(".j");
		
		Matcher m = p.matcher("aa");
		
		boolean b = m.matches();
		
		System.out.println(b);
		
		System.out.println(Pattern.matches("[abc]*", "ccc"));
		
		System.out.println(Pattern.matches("\\d*", "11122"));
		
		System.out.println(Pattern.matches("[a-zA-Z0-9]{8}", "Java1234"));
		
		String pwd = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$&])(?=\\S+$).{8,15}$";
		
		Pattern p1 = Pattern.compile(pwd);
		
		String input = "Java@1236";
		
		Matcher m1 = p1.matcher(input);
		
		boolean b1 = m1.matches();
		
		System.out.println(b1);
	}

}
