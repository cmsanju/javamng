package com.test;

class Student
{
	public Student()
	{
		System.out.println("constructor");
	}
	
	static
	{
		System.out.println("static");
	}
	
	{
		System.out.println("instance");
	}
	
	public void disp(String str)
	{
		System.out.println("String");
	}
	
	public void disp(Student obj)
	{
		System.out.println("object");
	}
}

public class Exp9 {
	
	public static void main(String[] args) {
		
		System.out.println("main");
		
		Student obj = new Student();
		
		//obj.disp(null);
	}

}
