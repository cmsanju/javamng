package com.test;


class Customer
{
	public Customer()
	{
		this(20);
		System.out.println("default");
	}
	
	public Customer(int x)
	{
		this("hello");
		System.out.println("single arg int");
	}
	
	public Customer(String str)
	{
		//this();
		System.out.println("string args");
	}
	
	public Customer(Object obj)
	{
		System.out.println("object");
	}
}

public class Exp10 {
	
	public static void main(String[] args) {
		
		Customer obj = new Customer(null);
		
		
	}

}
