package com.excp;

public class Exp1 {
	
	public static void main(String[] args) {
		
		try {
		
		System.out.println("Java");
		
		System.out.println(111/2);
		
		String str = "java";
		
		System.out.println(str.charAt(2));
		
		System.out.println("hello");
		
		int[] ar = {10,20,30};
		
		System.out.println(ar[1]);
		
		String name = null;
		
		System.out.println(name.charAt(0));
		
		}
		catch(ArithmeticException ae)
		{
			System.out.println("can't divided by zero");
		}
		catch(StringIndexOutOfBoundsException sie)
		{
			System.out.println("check your string length");
		}
		catch(ArrayIndexOutOfBoundsException aie)
		{
			System.out.println("check array size");
		}
		catch(NullPointerException npe)
		{
			System.out.println("please enter name");
		}
		catch(Exception e)
		{
			System.out.println("Check your inputs");
		}
		finally
		{
			System.out.println("i am from finally.");
		}
	}

}
/*
 
  try block
  catch block
  finally block
  
  throws
  throw

*/