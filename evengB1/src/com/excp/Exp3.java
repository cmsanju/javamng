package com.excp;

public class Exp3 {
	
	public static void main(String[] args) {
		
		try {
		
		System.out.println(100/0);
		
		}
		catch(Exception e)
		{
			//1 getMessage() only message
			System.out.println(e.getMessage());
			
			//2 exception class object gives class name and message
			
			System.out.println(e);
			
			//3 using printStackTrace() it will give exception class name message and line number
			
			e.printStackTrace();
		}
	}

}
