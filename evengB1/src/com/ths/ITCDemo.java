package com.ths;

class Item 
{
	int value;
	boolean valSet = false;
	
	public synchronized void putItem(int i)
	{
		try {
				if(valSet)
				{
					wait();
				}
				
				value = i;
				
				System.out.print("Producer produced item -> "+value);
				
				valSet = true;
				
				notify();
				
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public synchronized void getItem()
	{
		try
		{
			if(!valSet)
			{
				wait();
			}
			
			System.out.println(" -> Consumer consumed item -> "+value);
			
			valSet = false;
			
			notify();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
}

class Producer extends Thread
{
	int i;
	Item item;
	
	public Producer(Item item)
	{
		this.item = item;
	}
	
	public void run()
	{
		try
		{
			while(true)
			{
				Thread.sleep(1000);
				item.putItem(i++);
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
}

class Consumer extends Thread
{
	Item item;
	
	public Consumer(Item item)
	{
		this.item = item;
	}
	
	public void run()
	{
		try
		{
			while(true)
			{
				Thread.sleep(500);
				item.getItem();
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
}


public class ITCDemo {
	
	public static void main(String[] args) {
		
		Item item  = new Item();
		
		Producer pr = new Producer(item);
		Consumer cr = new Consumer(item);
		
		pr.start();
		cr.start();
	}

}
