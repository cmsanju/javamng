package com.ths;

public class Exp1 extends Thread
{
	
	public void run()
	{
		System.out.println("I AM FROM RUN METHOD");
	}
	
	public static void main(String[] args) {
		
		Exp1 t1 = new Exp1();
		
		Exp1 t2 = new Exp1();
		
		Exp1 t3 = new Exp1();
		
		System.out.println("Default thread name : "+t1.getName());
		System.out.println("Default thread name : "+t2.getName());
		System.out.println("Default thread name : "+t3.getName());
		
		t1.setName("open AC");
		t2.setName("withdraw");
		t3.setName("transfer");
		
		System.out.println("thread name : "+t1.getName());
		System.out.println("thread name : "+t2.getName());
		System.out.println("thread name : "+t3.getName());
		
		System.out.println("default thread priority : "+t1.getPriority());
		System.out.println("default thread priority : "+t2.getPriority());
		System.out.println("default thread priority : "+t3.getPriority());
		
		System.out.println(MAX_PRIORITY);
		System.out.println(NORM_PRIORITY);
		System.out.println(MIN_PRIORITY);
		
		t1.setPriority(MAX_PRIORITY);
		t3.setPriority(MIN_PRIORITY);
		
		System.out.println("thread priority : "+t1.getPriority());
		System.out.println("thread priority : "+t2.getPriority());
		System.out.println("thread priority : "+t3.getPriority());
	}

}
