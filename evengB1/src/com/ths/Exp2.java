package com.ths;

public class Exp2 implements Runnable
{
	@Override
	public void run()
	{
		System.out.println("i am from run() : "+Thread.currentThread().getName());
	}
	
	
	public static void main(String[] args) {
		
		Exp2 t1 = new Exp2();
		
		//t1.start();
		
		ThreadGroup tg1 = new ThreadGroup("Bank");
		
		Thread t2 = new Thread(tg1, t1,"pen AC");
		Thread t3 = new Thread(tg1, t1, "withdraw");
		Thread t4 = new Thread(tg1, t1, "transfer");
		
		ThreadGroup tg2 = new ThreadGroup("Maths");
				
		Thread t5 = new Thread(tg2, t1, "ADD");
		Thread t6 = new Thread(tg2, t1, "SUB");
		Thread t7 = new Thread(tg2, t1, "DIV");
		
		t4.start();
		t6.start();
		System.out.println("Thread group 1 : "+tg1.activeCount());
		System.out.println("Thread group 2 : "+tg2.activeCount());
		
		System.out.println(tg1);
	}
}
