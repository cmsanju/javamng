package com.fls;

import java.io.FileWriter;

public class WriteData {
	
	public static void main(String[] args) throws Exception
	{
		
		FileWriter fw = new FileWriter("src/write.txt");
		
		String msg = "This is char stream file write operation";
		
		fw.write(msg);
		
		fw.flush();
		
		System.out.println("done.");
	}

}
