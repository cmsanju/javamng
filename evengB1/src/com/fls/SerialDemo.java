package com.fls;

import java.io.FileOutputStream;
import java.io.ObjectOutputStream;

public class SerialDemo {
	
	public static void main(String[] args) throws Exception
	{
		
		FileOutputStream fos = new FileOutputStream("src/customer.txt");
		
		ObjectOutputStream os = new ObjectOutputStream(fos);
		
		Customer cc = new Customer();
		
		cc.id = 11;
		cc.name = "Hero";
		cc.city = "Blr";
		cc.pin = 560069;
		
		os.writeObject(cc);
		
		System.out.println("done.");
	}

}
