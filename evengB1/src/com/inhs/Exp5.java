package com.inhs;

@FunctionalInterface
interface FunTest
{
	void message();
	
}

public class Exp5 {
	
	public static void main(String[] args) {
		
		FunTest obj = new FunTest()
				{
					public void message()
					{
						System.out.println("overrided");
					}
				};
				
				obj.message();
		
				// JDK 1.8
		FunTest obj1 = () -> {
			
			System.out.println("overrided lambda");
		};
		
		obj1.message();
	}

}

/*
 * 
marker or tagged interfaces 
1 Cloneable 
2 Serializable 
3 Remote etc.

*/

