package com.inhs;

interface Inf2
{
	interface Inf3
	{
		
	 void sub();
	}
	
	void add();
}

class Impl1 implements Inf2
{
	public void add()
	{
		System.out.println("overrided");
	}
}

public class Exp4 {
	
	public static void main(String[] args) {
		
		Impl1 obj = new Impl1();
		
		obj.add();
				
	}

}
