package com.inhs;

class A
{
	int id = 2;
	String name = "Java";
	
	public void disp() {
		
		System.out.println("parent");
	}
}

class B extends A // IS - A
{
	String city = "Blr";
	
	public void details()
	{
		System.out.println(id+" "+name+" "+city);
	}
}
class C extends B
{
	public void pet()
	{
		
	}
}

class D extends C
{
	public void cat()
	{
		
	}
}

public class Exp1 {
	
	B obj = new B();//HAS - A
	
	
	public static void main(String[] args) {
		
		B b = new B();// USES - A
		
		b.disp();
		b.details();
	}

}
