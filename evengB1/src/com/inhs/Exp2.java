package com.inhs;

class E 
{
	public void draw()
	{
		System.out.println("tle");
	}
}

class F extends E
{
	@Override
	public void draw()
	{
		System.out.println("Rle");
	}
}

class G extends E
{
	@Override
	public void draw()
	{
		System.out.println("Cle");
	}
}

public class Exp2 {
	
	public static void main(String[] args) {
		
		F f = new F();
		
		f.draw();
		
		G g = new G();
		
		g.draw();
		
		E obj1 = new F();
		
		obj1.draw();
		
		E obj2 = new G();
		
		obj2.draw();
		
	}

}
