package com.inhs;

interface Inf1
{
	void disp();//abstract method or unimplemented method
	
	default void show()
	{
		System.out.println("default");
	}
	
	static void details()
	{
		System.out.println("static");
	}
	
}

abstract class Abs
{
	
	public abstract void human();
	
	public void pet()
	{
		System.out.println("pet()");
	}
	
}


class Impl extends Abs implements Inf1
{
	
	public void human()
	{
		System.out.println("abs overrided");
	}
	
	public void disp()
	{
		System.out.println("inf overrided");
	}
}


public class Exp3 {
	
	public static void main(String[] args) {
		
		Impl obj = new Impl();
		
		obj.disp();
		obj.human();
		obj.pet();
		obj.show();
		
		Inf1.details();
		
		//Abs a = new Abs();
		
	}

}
