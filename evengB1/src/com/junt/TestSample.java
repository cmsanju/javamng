package com.junt;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class TestSample {
	
	Sample obj;
	
	@BeforeAll
	public static void beforeClass()
	{
		System.out.println("before all test cases");
	}
	
	@AfterAll
	public static void afterClass()
	{
		System.out.println("after all test cases");
	}
	
	@BeforeEach
	public void setUp()
	{
		System.out.println("before test method");
		
		obj = new Sample();
	}
	
	@AfterEach
	public void setDown()
	{
		System.out.println("after test method");
		
		obj = null;
	}
	
	@Test
	public void testAdd()
	{
		System.out.println("test add method");
		
		assertEquals(40, obj.add(20, 20));
	}
	
	@Test
	public void testSub()
	{
		System.out.println("test sub method");
		assertEquals(10, obj.sub(30, 20));
	}
	@Test
	public void testMul()
	{
		System.out.println("test mul method");
		assertEquals(20, obj.mul(10, 2));
	}
	@Test
	public void testGreet()
	{
		System.out.println("test greet method");
		
		assertEquals("hi hello", obj.greet("hi hello"));
	}

}
