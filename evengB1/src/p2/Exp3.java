package p2;

import p1.Exp1;

public class Exp3 {
	
	Exp1 obj = new Exp1();
	
	public void disp()
	  {
		 // System.out.println(obj.a);
		 // System.out.println(obj.b);
		 // System.out.println(obj.c);
		  
		  System.out.println(obj.d);
	  }

}

class Exp4 extends Exp1
{
	public void disp()
	  {
		//  System.out.println(a);//private 
		//  System.out.println(b);//default
		  
		  System.err.println(c);
		  System.out.println(d);
	  }
}

//System is class 
//out is an object of PrintStream class
//println method is overloaded inside the PrintStream class
